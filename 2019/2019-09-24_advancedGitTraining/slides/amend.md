# Amend a commit

* Enables to change a commit on `HEAD` (the last commit)

* Start by creating and committing a file in the `attendees` directory:
```bash
$ cd attendees
$ echo "# Firstname Lastname" > myName.md
$ git add myName.md
$ git commit -m "Add firstname lastname to attendees"
```

* Check the commit in the `log`:
```bash
$ git log
```



# Example 1: change the commit message

* Verify that your staging area is clear:
```bash
$ git status
```

* Use `git commit --amend` to change the commit

* Alternatively, you can use the `-m` flag to edit only the commit message:
    ```bash
    $ git commit --amend -m "Add title"
    ```

* Check the commit message in the `log`:
```bash
$ git log
```



# Example 2: change the commit content

* In your editor, change the text in the `myName.md` file. For instance:
```bash
My biography ...
```

* You can see that the file changed:
```bash
$ git status
```

* With the changes staged use the following command to commit the changes into the previous commit:
```bash
$ git add myName.md
$ git commit --amend --no-edit
```

* Check the commit content:
```bash
$ git show HEAD
```

* This will create and commit a new commit with the staged changes added and the same commit message.
