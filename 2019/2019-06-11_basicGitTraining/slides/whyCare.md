# Why you should care (1)

<div align="center">
<video width="40%" controls>
  <source src="slides/img/arianeLaunch.mov" type="video/mp4">
Your browser does not support the video tag.
</video>
</div>

**Aerospace Application**: First launch of Ariane 5, 1996

<font color="#A52A2A"><span class="fas fa-times-circle"></span></font> **Problem**: Rocket exploded shortly after liftoff.

<font color="#FFA500"><span class="fas fa-exclamation-triangle"></span></font> **Root cause of disaster - Software error:**

Horizontal velocity of the rocket exceeded the limits of the 16-bit signed integer value and led to overflow
Ariane 5 launched with the same software as Ariane 4 - **the code had not been tested**



# Why you should care (2)

<div align="center">
<img src="slides/img/therac.png">
</div>

**Biotechnology Application**: Therac-25 – computer controlled radiation therapy machine

<font color="#A52A2A"><span class="fas fa-times-circle"></span></font> **Problem**: Device malfunctions and delivers a lethal dose of radiation - several patients died.

<font color="#FFA500"><span class="fas fa-exclamation-triangle"></span></font> **Root cause of disaster - Software error:**

**No testing** of software before release, **lack of documentation**
