# R3.school

## June 11th, 2019

<div style="top: 6em; left: 0%; position: absolute;">
    <img src="theme/img/lcsb_bg.png">
</div>

<div style="top: 5em; left: 60%; position: absolute;">
    <img src="slides/img/r3-training-logo.png" height="200px">
    <br><br><br>
    <h1>git training for absolute beginners</h1>
    <br><br><br><br>
    <h4>
        Laurent Heirendt, Ph.D.<br><br>
        laurent.heirendt@uni.lu<br><br>
        <i>Luxembourg Centre for Systems Biomedicine</i>
    </h4>
</div>
