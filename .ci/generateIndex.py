import os, re
from os import path

def build_link(title, href):
    return f'- <a href="{href}">{title}</a>\n'

def build_section_start(title):
    return f'\n' #<h3>{title}</h3>\n

def build_section_end():
    return "" #\t\t</ul>\n\t</div>

def camel_to_snake(name):
  name = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
  return re.sub('([a-z0-9])([A-Z])', r'\1_\2', name).lower()

# loop through the entire internal tree
localroot = os.getcwd()


# generate the index properly speaking
cardDirs = ["2021", "2020", "2019"]
index=''

# determine first the directories
for direct in cardDirs:
    if path.isdir(direct):
        dirs = os.listdir(direct)
        dirs.sort(reverse=True);
        index += "\n# " + direct + "\n\n"
        for d in dirs:
            if d[0] != ".":
                href = 'https://courses.lcsb.uni.lu/'+d
                linkText = camel_to_snake(d)
                linkText = linkText.replace("_", " ")
                linkText = linkText.title()

                # special rules
                linkText = linkText.replace("Dm", "Data Management")
                linkText = linkText.replace("It", "IT")
                linkText = linkText.replace("Siu", "SIU")

                index += build_link(linkText, href)

# output the index
print(index)


# Read in the file
repoName = "courses.lcsb.uni.lu"
os.chdir(repoName)
indexFile = "index.md"
filedata = ""
with open(indexFile, 'r') as file :
    for line in file:
        filedata += line

        # stop reading once the index place holder has been reached
        if re.search("<!-- index -->", line):
            filedata += "[[ index ]]"
            break

# Replace the target string
filedata = filedata.replace('[[ index ]]', index)

# Write the file out again
with open(indexFile, 'w') as file:
  file.write(filedata)

print("\n > New index generated and saved in " + indexFile)
os.chdir("..")