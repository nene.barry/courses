#!/bin/bash

PAGEURL="https://courses.lcsb.uni.lu"

rootDir=$(pwd)
echo $rootDir

# clean previous builds
rm -rf $rootDir/public
rm -rf $rootDir/theme
mkdir $rootDir/public

if [[ $CI_COMMIT_BRANCH == "develop" ]]; then
    version="latest";
elif [[ $CI_COMMIT_BRANCH == "master" ]]; then
    version="stable";
else
    version="unstable";
fi

# build the unstable version - can be optimized
git submodule update --init --recursive

# add matomo
sed -i "/matomohead/r./.ci/analytics/head.html" theme/templates/_index.html
sed -i "/matomopolicy/r./.ci/analytics/policy.html" theme/templates/_index.html
sed -i "/matomobanner/r./.ci/analytics/gdpr-banner.html" theme/templates/_index.html

echo " > LAP included"

# loop through all the presentations
nBlades=0
for d in $(find . -maxdepth 1 -type d)
do
    if [[ "${d}" == *20* ]]; then
        cd "${d}"
        echo "changed to ${d}"
        for d1 in $(find . -maxdepth 1 -type d)
        do
            if [[ ${#d1} -ge 2 ]]; then
                let "nBlades+=1"
                BLADE=${d1:2}
                echo " > -----------------------------------------------------"
                echo "Building ... ${d1}"

                # create the required folders in public
                mkdir -p $rootDir/public/$BLADE/$version

                # change to the blade directory to be built
                cd $BLADE

                # run contribute
                cp ../../template/Gruntfile.coffee .
                ln -s ../../theme/package.json package.json
                ln -s ../../theme theme

                # install npm
                yarn global add grunt-cli generator-reveal
                yarn install

                grunt dist
                mv public/* $rootDir/public/$BLADE/$version/.

                echo " > latest built for file set ${BLADE}"

                # auto redirect file to the stable version
                echo '<html>\n
                    <head>\n
                    <meta http-equiv="refresh" content="0; url='$PAGEURL'/'$BLADE'/stable"/>\n
                    </head>\n
                    <body>\n
                    </bod>\n
                    </html>\n' > $rootDir/public/$BLADE/index.html

                echo "autoredirect file set"

                # move back out
                cd ..
                echo " > -----------------------------------------------------"
            fi
        done
        cd "${rootDir}"
    fi
done
