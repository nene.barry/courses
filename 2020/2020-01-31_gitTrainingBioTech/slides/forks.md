# What is a `fork`?

<br><br>
<center><img src="slides/img/fork.jpg" class="as-is" height="500em"/></center>
<!--http://www.cndajin.com/data/wls/246/22302193.jpg-->



# Not really ...

<br><br>
<center><img src="slides/img/fork-crossed.png" class="as-is" height="500em"/></center>



# What is a `fork`?

- In general, when contributing to a repository, you only have **read** access.

- In other words, you can only **pull** (unless it is your own repository or access has been granted).

- In general, you **cannot write** changes. In other words, you do not have **push** access.

- You have to work on your **own copy**.

- In other words, you have to work on your own <font color="red">**fork**</font>.

<br>
<h2>How to get a fork?</h1>

Browse to the original repository and click on the button `Fork`:

![Fork the repo](https://help.github.com/assets/images/help/repository/fork_button.jpg)




# How to update my fork?

As you have your own fork, it will not automatically be updated once the original repository is updated.

![bulb](slides/img/bulb.png) You have to update it yourself!

<img src="slides/img/icon-live-demo.png" height="100px">

**More on that during the demo!**




# Time to practice!

Fork the howto-cards (internal) repository: <br><br>
https://git-r3lab.uni.lu/R3-core/howto-cards-internal/

<img src="slides/img/icon-live-demo.png" height="100px">