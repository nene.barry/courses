# Let's refresh our memories

<div class="fragment">

- What is a **fork**?

<div class="fragment">

- What are **branches**?

<div class="fragment">

- Can I have **multiple branches** in my fork?

<div class="fragment">

- What is a good **development scheme**?




# Thank you.

<img src="slides/img/r3-training-logo.png" height="200px">

Contact us if you need help:

lcsb-r3@uni.lu

