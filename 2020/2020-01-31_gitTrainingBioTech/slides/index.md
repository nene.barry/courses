# R3.school

## January 31st, 2020

<div style="top: 6em; left: 0%; position: absolute;">
    <img src="theme/img/lcsb_bg.png">
</div>

<div style="top: 5em; left: 60%; position: absolute;">
    <img src="slides/img/r3-training-logo.png" height="200px">
    <br><br><br>
    <h1>git training for biotech team</h1>
    <br><br><br><br>
    <h4>
        Laurent Heirendt, Ph.D.<br>
        laurent.heirendt@uni.lu<br>
        <i>Luxembourg Centre for Systems Biomedicine</i>
    </h4>
</div>
