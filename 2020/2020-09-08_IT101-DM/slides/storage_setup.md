# Storage set-up

* Download Anti-virus software
* Regualarly update your SW/OS
* Encrypt movable media

### Passwords

* Strong passwords
* Password manager
* Safe password exchange channels
* Expiration time on password share

### Backup
  * take care of your own backups!
  * don't work on your backup copy!
  * minimum is <b>3-2-1 backup rule</b>


<div style="position:absolute;right:10%;top:10%">
<img src="slides/img/undraw_secure_server_s9u8.png" height="750px">
</div>
<div style="position:absolute; width:45%; left:50%; top:28em; text-align:right">
<a href=" https://howto.lcsb.uni.lu/?policies:LCSB-POL-BIC-02" style="color:grey; font-size:0.8em;">Data Storage and Backup Policy</a>
</div>



# Storage set-up
## Backup - Central IT/LCSB
<div style="position:relative">
<img src="slides/img/LCSB_storages_backed-up.png" height="750px">
</div>
<div style="position:absolute;left:65%;top:60%">

Server administrators take care of:
* server backups
* LCSB OwnCloud backups
* group/application server backups (not always)

</div>



# Storage set-up
## Backup - personal research data
<div style="position:relative">
<img src="slides/img/LCSB_storages_backup.png" height="750px">
</div>
<div style="position:absolute;left:55%;top:70%">

<font color="red">One version should reside on Atlas!</font>

</div>
