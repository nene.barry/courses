# Let's refresh our memories

<div class="fragment">

- What is a **fork**?

<div class="fragment">

- What are **branches**?

<div class="fragment">

- Can I have **multiple branches** in my fork?

<div class="fragment">

- What is a good **development scheme**?

<div class="fragment">

- What are the **5 essential commands**?



# References & Cheat sheet

1. Git Book: https://git-scm.com/book/en/v2

2. Cheat sheet: http://rogerdudler.github.io/git-guide



# Thank you.

<img src="slides/img/r3-training-logo.png" height="200px">

Contact us if you need help:

lcsb-r3@uni.lu

