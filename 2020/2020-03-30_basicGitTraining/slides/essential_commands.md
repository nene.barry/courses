# The 5 essential commands


**Yes**, you only need 5 commands!

`pull, status, add, commit, push`

or in other words (remember these!):
```bash
$ git pull <remote> <branch>
$ git status
$ git add myFile.md # example
$ git commit -m "myMessage" # example
$ git push <remote> <branch>
```



# Pull the latest version of an existing branch

Pull the latest revision on branch `myBranch`:
```bash
$ git pull origin myBranch
# Already up to date
```

<div class="fragment">

Verify its `status` with:
```bash
$ git status
```



# Modify a file

Copy the file `template.md` in the folder `attendees` and rename it with your firstname:

```bash
$ cd attendees
$ cp firstnameLastname.md myName.md
```

Then, make your changes with your favorite editor!



# Add your file to the stage

First, check the repository status
```bash
$ git status
# uncommitted changes (displayed in red)
```


<div class="fragment">

Now, add the file (bring it on stage)
```bash
$ git add myName.md # replace myName
$ git status
# returns the same as before, generally in green (means staged)
```

<div class="fragment">

**ADVANCED**: If there have been more changes after the file has been added, you can see your changes in the terminal
```bash
$ git diff
```
exit with `q`



# Add a commit message

```bash
$ git commit -m "Add the profile of <myName>"
$ git status
```



# Push your file to your fork

```bash
$ git push origin myBranch
```

<div class="fragment">

**ADVANCED**: see the log of all the commits (and your last one) in the terminal
```bash
$ git log
```
exit by typing `q`.