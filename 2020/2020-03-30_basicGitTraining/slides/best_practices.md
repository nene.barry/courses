# Best practices

* `pull` before `push`
* Work on your <font color="red">own</font> branch (in your own fork), and **not** on `master` and **not** on `develop`
* Do **not push** to `master`, but **submit a Merge Request (MR)**
* Get your code **reviewed** by your peers (submit a MR!)
* Submit a MR **often**!
* `clone` a repository, do not download the `.zip` file.
* Do **not** combine `git` commands
   ```bash
   $ git commit -am "myMessage" # do not do this
   ```
* Stage only 1 file at once using
   ```bash
   $ git add myFile.md
   ```
* Commit **only a few files** at once (after multiple separate `git add` commands)
* `Push` often - avoid conflicts

Remember: **A `push` a day keeps conflicts away!**
