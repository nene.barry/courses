## Overview

0. Introduction - learning objectives + targeted audience
1. Data workflow
1. Ingestion:
    * receiving/sending/sharing data
    * file naming
    * checksums
    * backup
 1. making data tidy 
    * what is table 
    * 
 1. Learning to code workflows and analyses - excel files, coding
 1. Code versioning and reproducibility
 1. Visualization
  * see the data
 1. problem solving
    * guide
    * rubberducking
    * google for help
    * oracle
  1. R3 team
  1. Acknowledgment
  
  1. data minimization
