

# Prerequisites

-   SSH
-   SSH Clients
-   Client installation Win, Mac
-   Logging into a \*nix server
    -   Logging in with password
    -   Passwordless login
        -   RSA key generation
        -   Port forwarding (?)
-   VNC Clients (?)


# LCSB's  Credentials Management System : LUMS

-   Key management


# Effective shell usage

Overview of the capabilities and possibilities.


## Basics


### At the gates

-   ls/cd/mv/rm/cp
-   scp (rsync?)


### Documentation Systems

-   info
-   man


### Linux Filesystem Structure

-   home,dev,proc,etc,usr
-   ownership (users and groups)
-   read, write, execute permissions
-   escalating priviliedge


### Environment

-   environment variables
-   PATH, HOME
-   .profile and friends
-   source (.)


## Effective use


### History commands

-   GNU readline
-   !, !!, !-N, ^&#x2026;^, &#x2026;.


### nano


### find


### grep


### sed


### awk

