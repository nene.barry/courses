# Resources

## Day to Day Shell System Use

* [Advanced Bash Scripting Guide](https://tldp.org/LDP/abs/html/)

A text with misleading title, it is neither only meant for _Advanced_
users as it assumes no previous exposure to the Unix shell, nor covers
only _Bash_, taking instead an integrative approach which reflects a
natural interaction with a GNU/Linux system on a day-to-day basis. It
can also serve as a reference. While outdated in the sense that that
last revision is from 2014, it still covers the vast majority of
interactions one has with a GNU/Linux system through the shell.

* [Software Carpentry Courses](https://v4.software-carpentry.org/shell/index.html)

Plenty of well organised self-study friendly courses. The link above
is for the Unix shell.



* [Generating SSH Keys on Windows using PuTTY](https://lcsb-wiki.uni.lu/xwiki/wiki/eci/view/Generating+SSH+Accounts/)

This is a wiki page by MSc students of the **LCSB Environmental
Cheminformatics Group** which documents the process of SSH key
generation on the Windows machines.


* [UL HPC School Resources for Beginners](https://ulhpc-tutorials.readthedocs.io/en/latest/beginners/)

This shell and server access tutorial by the UL HPC platform is one of
their [many excellent learning resources](https://ulhpc-tutorials.readthedocs.io/en/latest/).


## Miscellaneous

* [The GNU in GNU/Linux](www.gnu.org) The system/organisation/movement
  that started (much of) it all. Free software, [the philosophy and  ethics of it](https://www.gnu.org/philosophy/philosophy.html).

* [The Linux Project](https://www.linux.org/)
  The official site of Linux. Also has a wealth of resources.

* [The Open Source Initiative](https://opensource.org/)
  A business-friendly take on Free Software. For those interested in historic minutiae, 
  see [OSI vs FSF](https://en.wikipedia.org/wiki/Open_Source_Initiative#Relationship_with_the_free_software_movement).

## Windows Tools Related to Gnu/Linux

* [PuTTY](https://putty.org/)
  SSH client, agent and GPG generator.
  
* [VNC](https://tigervnc.org/)
  One of many VNC clients.
  
* [Cygwin](https://www.cygwin.com/) 
  A venerable collection of GNU and Open Source tools which provide
  functionality similar to a Linux distribution on Windows.
  
* [MobaXterm](https://mobaxterm.mobatek.net/) 
  A more sleek variant of _Cygwin_. Freeware (for personal use), not
  open source.
