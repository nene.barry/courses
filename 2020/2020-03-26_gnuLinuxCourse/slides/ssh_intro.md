# Accessing Computers over Network (Securely)


* Computer is a communication device

<!-- .element: class="fragment" -->
* It sends and receives at its abstract entry/exit points aptly called [ports][1]

  - there are plenty of them (it would have made more sense to call
    them piers where a single computer would be a port, but alas
    ... :) )
	
  <!-- .element: class="fragment" -->

<!-- .element: class="fragment" -->
* Various programs can listen for, or send through messages
  (i.e. data) via ports continuously, or in specified limited
  intervals ...

<!-- .element: class="fragment" -->

[1]: https://en.wikipedia.org/wiki/Port_(computer_networking)
