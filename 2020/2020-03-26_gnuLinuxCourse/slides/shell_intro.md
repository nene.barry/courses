# The Beginning

* We're in! <!-- .element: class="fragment"-->
  <!-- .element: class="fragment" data-fragment-index="1" -->
  ```bash
  todor@rad:~$ What now?
  Command 'What' not found ...
  ```
  <!-- .element: class="fragment" data-fragment-index="2" -->


* Elements of the command line
  <!-- .element: class="fragment" data-fragment-index="3" -->
	+ Can vary a lot between systems (and users)
	  <!-- .element: class="fragment" data-fragment-index="3" -->
	+ but, generally contains information about the current _user_ and
	  the _hostname_ (the name of the machine)
	  <!-- .element: class="fragment" data-fragment-index="3" -->
    + For an unpriviledged user, it usually ends in **$**
	  <!-- .element: class="fragment" data-fragment-index="3" -->



# Typing at the shell prompt (Readline Basics)  <!-- TODO: Possibly after a few simple command examples -->
* Command execute by pressing `Return` 
* Move a single character using `Left` and `Right` arrow keys
* Use `Backspace` to remove the preceding character
* Move through history of commands using `Up` and `Down` keys
<!-- TODO: Possibly after some more complicated commands -->
* Jump between words using `Alt`/`Ctrl` in combination with `Left` and `Right`
* Use `Ctrl` + `d` to remove the character under cursor
* Delete an entire word using `Alt` + `d`
* A deleted word ends up in your _kill buffer_ : retrieve it with `Ctrl` + `w`

