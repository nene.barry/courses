# Let's refresh our memories

<div class="fragment">

- What is a **fork**?

<div class="fragment">

- What are **branches**?

<div class="fragment">

- Can I have **multiple branches** in my fork?

<div class="fragment">

- What is a good **development scheme**?

<div class="fragment">

- What are the **5 essential commands**?



# References & Cheat sheet

[1]: Git Book: https://git-scm.com/book/en/v2

[2]: GitHub training services: https://services.github.com/training/

[3]: Cheat sheet: http://rogerdudler.github.io/git-guide



# Thank you.

<br>
<center>
<img src="slides/img/elixir.png" height="200px">
<img src="slides/img/r3-training-logo.png" height="200px">

<br><br>
Contact us if you need help:
<br><br>
lcsb-r3@uni.lu
<br><br>
We'd appreciate feedback: https://is.gd/gitelixir202103
<br><br>
Next course: `High-performance scientific computing using Julia on April 22nd, 2021`

</center>