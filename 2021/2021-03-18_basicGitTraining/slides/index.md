# Elixir-LU training (R3.school)

## March 18th, 2021

<div style="top: 6em; left: 0%; position: absolute;">
    <img src="theme/img/lcsb_bg.png">
</div>

<div style="top: 5em; left: 60%; position: absolute;">
    <img src="slides/img/elixir.png" height="200px">
    <img src="slides/img/r3-training-logo.png" height="200px">
    <br><br><br><br>
    <h1>Basic git training</h1>
    <br><br><br><br>
    <h4>
        Laurent Heirendt, Ph.D.<br>
        laurent.heirendt@uni.lu<br>
        <i>ELIXIR/LU, Luxembourg Centre for Systems Biomedicine</i>
    </h4>
</div>

