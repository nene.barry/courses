# How do I start working on a repository?

You have to `clone` it first:

```bash
$ git clone git@github.com:LCSB-BioCore/basic-git-practice.git
```

If you did not configure your SSH key, clone using HTTPS:
```bash
$ git clone https://github.com/LCSB-BioCore/basic-git-practice.git
```

You will be prompted to enter your credentials.
