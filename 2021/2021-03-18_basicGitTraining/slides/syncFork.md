# Synchronize your fork

![bulb](slides/img/bulb.png) Remember, we have to regularly update our own copy of the code.


Add the `upstream` address (original/protected repository)
```bash
$ git remote add upstream git@github.com:LCSB-BioCore/basic-git-practice.git
```

![bulb](slides/img/bulb.png) Note the change in the URL.


You can then check whether the remote address is set correctly
```bash
$ git remote -v
```

<div class="fragment">

Fetch the changes from upstream (similar to pull)
```bash
$ git fetch upstream
```




Merge the retrieved changes on the `master` branch:
```bash
$ git checkout master
$ git merge upstream/master
$ git push origin master
```

<div class="fragment">

Do the same for the `develop` branch:

```bash
$ git checkout develop
$ git merge upstream/develop
$ git push origin develop
```

<img src="slides/img/icon-live-demo.png" height="100px">