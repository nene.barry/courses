# First steps in the terminal

Starting the terminal presents itself with a line where you can enter a command
```bash
cesar@myComputer>
```

Often written, for covenience, as
```bash
$
```

When you open your terminal (shell), you are located
in your home directory (unless otherwise configured), denoted as `~/`.

<br>

**Is `git` properly installed?**

```bash
$ git --version
# git version 2.10.0
```



# Essential Linux commands

List the contents of a directory
```bash
$ ls
```

You can list more details with:
```bash
$ ls -lah
```

Create a directory
```bash
$ mkdir myNewDirectory
```



Change the directory to a specific folder
```bash
$ cd myNewDirectory
```

Change the directory 1 level and 2 levels up
```bash
$ cd ..
# 1 level up

$ cd ../..
# 2 levels up
```

Move a file or a directory
```bash
$ mv myFile.m myNewDirectory/.
```


Rename a file or a directory
```bash
$ mv myFile.m myNewFile.m
$ mv myNewDirectory myDirectory
```