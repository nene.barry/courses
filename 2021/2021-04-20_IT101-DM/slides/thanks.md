# Thank you.<sup> </sup>
<center><img src="slides/img/r3-training-logo.png" height="200px"></center>
<br>
<br>
<br>
<br>
<center>
Contact us if you need help:

<a href="mailto:lcsb-r3@uni.lu">lcsb-r3@uni.lu</a>
</center>
<div style="position:absolute">
Links:

HowTo Cards / Policies: https://howto.lcsb.uni.lu/

Course Slides: https://courses.lcsb.uni.lu/

Internal Presentations: https://presentations.lcsb.uni.lu/

LCSB GitLab: https://git-r3lab.uni.lu/

HPC: https://hpc.uni.lu/

Service Portal: https://service.uni.lu/sp

LCSB intranet: https://intranet.uni.lux
</div>
<div style="position:relative;top:1.5em;left:55%;width:45%">
Avalable SW and tools:
<div style="margin-left: 20px;">
SIU managed:

&ensp; - Service Portal > All Catalogs > IT > Softwares
</div>
<div style="margin-left: 20px;">
LCSB managed:

&ensp; - Service Portal > Knowledge > FAQ - Corporate Software\
&ensp; - LCSB intranet > Science tab > Tools
</div>
</div>
