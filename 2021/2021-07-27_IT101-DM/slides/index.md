# IT101 - Working with computers
<br>IT101 - Working with computers<br>
## July 27th, 2021

<div style="top: 6em; left: 0%; position: absolute;">
    <img src="theme/img/lcsb_bg.png">
</div>

<div style="top: 5em; left: 60%; position: absolute;">
    <img src="slides/img/r3-training-logo.png" height="200px">
    <br><br><br><br>
    <h3></h3>
    <br><br><br>
    <h4>
        Nene Barry<br>
        Data Steward<br>
        nene.barry@uni.lu<br>
        <i>Luxembourg Centre for Systems Biomedicine</i>
    </h4>
</div>
