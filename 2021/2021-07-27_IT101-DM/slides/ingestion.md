# Data housekeeping
## Available data storage
<div class='fragment' style="position:absolute">
<img src="slides/img/LCSB_storages_full.png" height="750px">
</div>

<div class='fragment' style="position:relative">
<img src="slides/img/LCSB_storages_personal-crossed.png" height="750px">

<div style="position:absolute;left:65%;top:60%">

* Unless consortium/project has formally agreed to use a secure commercial cloud

</div>

</div>

<div style="position:absolute; width:45%; left:50%; top:28em; text-align:right">
<a href=" https://howto.lcsb.uni.lu/?policies:LCSB-POL-BIC-02" style="color:grey; font-size:0.8em;">Data Storage and Backup Policy</a>
</div>




# Data ingestion: Transfer and Integrity
  * When sending data: <font color="red">Do not use emails, use secure platforms (Cloud, Aspera, Atlas share...)!</font>

<div class="fragment">
Data can be corrupted:

  * (non-)malicious modification
  * faulty file transfer
  * disk corruption
</div>
  
<div class="fragment">

### Solution

 * disable write access to the source data
 * generate checksums!
 
<div style="position:absolute;left:40%;top:30%">
<img src="slides/img/checksum.png" width="500px">
</div>
</div>

<div class="fragment" style="position:relative; left:0%">


## When to generate checksums?
* before data transfer
  - new dataset from collaborator
  - upload to remote repository

* long term storage
  - master version of dataset
  - snapshot of data for publication
</div>

<div style="position:absolute; width:45%; left:50%; top:28em; text-align:right">
<a href=" https://howto.lcsb.uni.lu/?policies:LCSB-POL-BIC-02" style="color:grey; font-size:0.8em;">Data Storage and Backup Policy</a>
</div>



# Data ingestion/Integrity
## Encryption
<div class='fragment' style="position:relative;left:25%;top:60%">
<img  align="middle" height="300px" src="slides/img/encryption.png">
</div>

<div class='fragment'>

* Guaranted confidentiality
</div>
<div class='fragment'>

* Encryption key need to be kept safe
</div>
<div class='fragment'>

* <font color= red>Loosing your encryption key means loosing your data!</font>
</div>
<div class='fragment'>

* Make a off-site backup of your data
</div>


