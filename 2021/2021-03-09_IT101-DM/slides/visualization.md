# Visualization
<center>

**Plot your data!**
<figure>
  <img src="slides/img/DinoSequentialSmaller.gif" height="500px">
  <blockquote>"never trust summary statistics alone; always visualize your data"</blockquote>
  <figcaption>--Alberto Cairo</figcaption>
</figure> 
</center>



# Visualization
<center>

**Plot your data!**
<figure>
  <img src="slides/img/plot-data.png" height="800px">
</figure> 
</center>
