# Data housekeeping
## Available data storage
<div class='fragment' style="position:absolute">
<img src="slides/img/LCSB_storages_full.png" height="750px">
</div>

<div class='fragment' style="position:absolute">
<img src="slides/img/LCSB_storages_personal-crossed.png" height="750px">
</div>

<div style="position:absolute; width:45%; left:50%; top:28em; text-align:right">
<a href=" https://howto.lcsb.uni.lu/?policies:LCSB-POL-BIC-02" style="color:grey; font-size:0.8em;">Data Storage and Backup Policy</a>
</div>



# Data ingestion/transfer
## Receiving and sending data 

<img height="400px" style="position:relative;left:10%" src="slides/img/banned_exchange_channels.png"><br>
<div style="position:absolute; left:10%;width:30%">

## E-mail is not for data transfer

* Avoid transfer of any data by e-mail
* E-mail is a poor repository
* Data can be read on passage

</div>
<div class="fragment" style="left:50%; width:30%; position:absolute">

## Exchanging data
* Share on Atlas server
* OwnCloud share (LCSB - BioCore)
* DropIt service (SIU)
* LFT (IBM Aspera) share for sensitive data
</div>
</div>

<div style="position:absolute; width:45%; left:50%; top:28em; text-align:right">
<a href=" https://howto.lcsb.uni.lu/?policies:LCSB-POL-BIC-05" style="color:grey; font-size:0.8em;">Research Human Data Sharing Policy</a>
</div>



# Data ingestion/transfer
Data can be corrupted:
  * (non-)malicious modification
  * faulty file transfer
  * disk corruption
  
<div class="fragment">

### Solution

 * disable write access to the source data
 * generate checksums!

<div style="position:absolute;left:40%">
<img src="slides/img/checksum.png" width="500px">
</div>
</div>

<div class="fragment" style="position:relative; left:0%">


## When to generate checksums?
* before data transfer
  - new dataset from collaborator
  - upload to remote repository

* long term storage
  - master version of dataset
  - snapshot of data for publication
</div>

<div style="position:absolute; width:45%; left:50%; top:28em; text-align:right">
<a href=" https://howto.lcsb.uni.lu/?policies:LCSB-POL-BIC-02" style="color:grey; font-size:0.8em;">Data Storage and Backup Policy</a>
</div>
