#!/bin/bash

# set the variables
dockerImg="git-r3lab.uni.lu:4567/r3/docker/nodejs-yarn-grunt"
dockerPort="9000"

helpFunction()
{
   echo ""
   echo "Usage: $0 -d parameterDate -n parameterName"
   echo -e "\t-d Date of presentation"
   echo -e "\t-n Name of presentation"
   exit 1 # Exit script after printing help
}

while getopts "d:n:" opt
do
   case "$opt" in
      d ) parameterDate="$OPTARG" ;;
      n ) parameterName="$OPTARG" ;;
      ? ) helpFunction ;; # Print helpFunction in case parameter is non-existent
   esac
done

# Print helpFunction in case parameters are empty
if [ -z "$parameterDate" ] || [ -z "$parameterName" ]
then
   echo "Some or all of the parameters are empty";
   helpFunction
fi

# launch the docker file
cmd="python contribute.py --date=\"$parameterDate\" --name=\"$parameterName\""

docker run -p $dockerPort:$dockerPort --user $(id -u):$(id -g) -it -v $(pwd)/:/presentation $dockerImg /bin/bash -c "$cmd"
